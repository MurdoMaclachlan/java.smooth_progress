# java.smooth_progress

A java implementation of my smooth_progress project, originally written in Python. Original: 
https://codeberg.org/MurdoMaclachlan/smooth_progress

This was really just made as an exercise to practice Java.