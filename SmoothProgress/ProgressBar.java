package SmoothProgress;

/*
 * The primary class for the ProgressBar. All control operations exist as methods of
 * this class.
 */
public class ProgressBar {

    private int count;
    private static int GRANULARITY = 50;
    private static int LIMIT;
    private boolean opened;
    private boolean print;
    private boolean show_percent;
    private String state;

    public ProgressBar(int lim, boolean sp, boolean p) {
        init(lim, p, sp);
        open();
    }

    public ProgressBar(int lim, boolean sp) {
        init(lim, true, sp);
        open();
    }

    public ProgressBar(int lim) {
        init(lim, true, true);
        open();
    }

    public ProgressBar() {
        init(100, true, true);
        open();
    }

    private void init(int l, boolean p, boolean s) {
        opened = false;
        LIMIT = l;
        print = p;
        show_percent = s;
    }

    /*
     * Primary API
     */

    /*
     * Closes the ProgressBar from mutability, displaying its final state before
     * closure.
     * 
     * Note that a carriage return is executed BEFORE the final display; this ensures
     * console outputs such as ^C from a Control+C SIGKILL will be overwritten.
     */
    public boolean close() {
        if (opened) {
            display(0, "\r");
            System.out.print("\n");
            opened = false;
            return true;
        }
        return false;
    }

    /*
     * Increments the progress and updates the display to reflect the new value. If
     * this incrementation takes the progress to the pre-defined limit, closes the
     * ProgressBar from mutability.
     */
    public void increment() throws ProgressBarClosedError {
        if (opened) {
            count++;
            double fraction = (double) count / (double) LIMIT;
            update(fraction*GRANULARITY, fraction*100);
            display(1, "\r");
            if (count == LIMIT) {
                close();
            }
        } else {
            throw new ProgressBarClosedError(".increment()");
        }
    }

    /*
     * A more forceful version of close(); interrupts the ProgressBar by closing it
     * from mutability without displaying its final state.
     * 
     * @returns Boolean success status
     */
    public boolean interrupt() {
        if (opened) {
            opened = false;
            display(1, "\r");
            return true;
        }
        return false;
    }

    /*
     * Resets all progress and opens the ProgressBar to mutability, displaying its
     * initial, empty state.
     * 
     * @returns Boolean success status
     */
    public boolean open() {
        if (opened) {
            return false;
        } else {
            count = 0;
            update(0, 0);
            //state = "[" + new String(new char[GRANULARITY]).replace("\0", "-") + "}]  0/" + LIMIT;
            display(1, "\r");
            opened = true;
            return true;
        }
    }

    /*
     * Getters & setters
     */

    /*
     * Get the current state of the bar.
     * 
     * @returns state The state of the bar.
     */
    public String getState() {
        return state;
    }

    /*
     * Private helpers
     */

    /*
     * Helper method to display the current state of the ProgressBar.
     * 
     * @param pos The position of 'esc'; if 0, prepend to 'state', if 1, append.
     * @param esc Should be a control character, such as '\r', '\n'.
     */
    private void display(int pos, String con) {
        if (print) {
            System.out.print(pos == 0 ? con + state : state + con);
        }
    }

    /*
     * Helper method to update the state of the bar with new values.
     * 
     * @param completion The number of units on the bar to fill as completed
     * @param percentage The percentage of the bar that has been completed
     */
    private void update(double com, double per) {
        int completion = (int) com;
        int percentage = (int) per;
        state = (
            "["
            + new String(new char[completion]).replace("\0", "#")               // Progress complete
            + new String(new char[GRANULARITY - completion]).replace("\0", "-") // Progress to go
            + "]  "
            + count
            + "/"
            + LIMIT
            + (show_percent ? (" [" + percentage + "%]") : "")                   // Percent complete, if set to display
        );
    }
}