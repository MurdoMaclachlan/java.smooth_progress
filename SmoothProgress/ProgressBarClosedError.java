package SmoothProgress;

public class ProgressBarClosedError extends Exception {

    private String call;

    ProgressBarClosedError(String c) {
        super(c + " was called, but ProgressBar is closed.");
        call = c;
    }

    public boolean equalTo(ProgressBarClosedError other) {
        return call == other.call;
    }
}
